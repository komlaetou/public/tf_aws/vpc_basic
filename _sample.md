# Provider sample
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.3.0"
    }
  }
}

provider "aws" {
  region              = var.region
}


#------------------------------------------------------------
# SAMPLE VPC
#------------------------------------------------------------
module "vpc" {
  source = "git::https://gitlab.com/komlaetou/public/tf_aws/vpc_basic.git?ref=v1.0.0"
  vpc_name = var.vpc_name
  vpc_cidr = var.vpc_cidr
  tags = var.tags

}


# Variables
variable "vpc_name" {
  default = "mkc_lab_vpc"
}

variable "vpc_cidr" {
  default = "172.20.0.0/16"
}

variable "region" {
  default = "eu-west-1"
}

variable "igw" {
  description = "to create IGW"
  type = bool
  default = true
}


variable "environment" {
  type = string
  description = "prod or staging"
  default = null
}

variable "tags" {
    default = {
        Created_by = "mkc"
    }
  
}

# Output
output "vpc_details" {
  value = {
    vpc_id = module.vpc.vpc_details["vpc_id"]
    vpc_arn = module.vpc.vpc_details["vpc_arn"]
    igw_id = module.vpc.vpc_details["igw_id"]
  }
}
