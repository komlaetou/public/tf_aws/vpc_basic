locals {
  default_tags = {
    IACTool      = "terraform"
    Environment  = var.environment
  }

  tags            = merge(local.default_tags, var.tags)
}
